#include <iostream>
#include <string>

class Cliente {
	public:
	std::string nome;
	std::string cpf;
};

class Agencia {
	public:
	std::string nome;
	int numeroDaAgencia;
};

class Conta {
	public:
	int numero;
	Cliente cliente;
	Agencia agencia;
	double saldo;
	double limite;
	static int quantidade_contas;

	Conta() {
		this->quantidade_contas += 1;
	}

	Conta(const Conta &conta) {
		this->quantidade_contas += 1;
	}

	~Conta() {
		this->quantidade_contas -= 1;
	}

	double saca(double valor) {
		if (valor > this->limite) {
			std::cout << "Valor ultrapassa o limite!" << std::endl;
		}
		else if (valor > this->saldo) {
			std::cout << "Valor ultrapassa o saldo!" << std::endl;
		}
		else {
			this->saldo -= valor;
			std::cout << "Sacado com sucesso!" << std::endl;
			return valor;
		}
		return 0;
	}
	void deposita(double valor) {
		if (valor > this->limite) {
			std::cout << "Valor ultrapassa o limite!" << std::endl;
		}
		else {
			this->saldo += valor;
			std::cout << "Depositado com sucesso!" << std::endl;
		}
	}

	void transfere(double valor, Conta &conta) {
		if (valor > this->limite) {
			std::cout << "Valor ultrapassa o limite!" << std::endl;
		}
		else if (valor > this->saldo) {
			std::cout << "Valor ultrapassa o saldo!" << std::endl;
		}
		else {
			this->saldo -= valor;
			conta.saldo += valor;
			std::cout << "Transferido com sucesso!" << std::endl;
		}
	}
};

int Conta::quantidade_contas;

class Carro {
	public:
	std::string tipo;
	std::string cor;
	std::string placa;
	int numero_portas;
	static int quantidade_carros;

	Carro() {
		this->quantidade_carros++;
	}

	Carro(const Carro &carro) {
		this->quantidade_carros++;
	}

	~Carro() {
		this->quantidade_carros--;
	}

	void liga() {

	}

	void acelera() {

	}

	void freia() {

	}

	void pega_marcha() {

	}

	void abastecer() {

	}
};

int Carro::quantidade_carros;

int main(int argc, char *argv[]) {
	Conta conta;

	conta.numero = 321324;
	conta.cliente.nome = "Pedro";
	conta.cliente.cpf = "93283434";
	conta.agencia.nome = "Banco do Brasil";
	conta.agencia.numeroDaAgencia = 94;
	conta.saldo = 9238.99;
	conta.limite = 100.55;

	conta.saca(50.15);

	Conta conta2(conta);

	std::cout << "Quantidade de contas: " << conta.quantidade_contas << std::endl;

	Carro carro;
	carro.tipo = "Comum";
	carro.cor = "Verde";
	carro.placa = "USH-23423";
	carro.numero_portas = 2;

	Carro carro2;

	std::cout << "Quantidade de carros: " << carro.quantidade_carros << std::endl;

	return 0;
}